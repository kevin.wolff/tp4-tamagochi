////// VARIABLES HOME //////

let game = document.querySelector(".game");
let bgHome = document.querySelector(".bgHome");
let homeButton = document.querySelector(".homeButton");

// WHEN CLICK ON HOME BUTTON
homeButton.addEventListener("click", function() {

    // BG HOME DISAPEAR
    bgHome.style.display = "none"
    // GAME TCHAT APPEAR
    game.style.display = "block"
    // FUNCTION START PROGRESS BAR
    startProgressBar();
    // FUNCTION START DEAD
    idDead = setInterval(function() {
        dead(heart, coffee, code);
    }, 1000)
});

////// HEART PROGRESS BAR VARIABLES //////

let widthHeart = 100;
let progressHeart = document.querySelector(".progressHeart");
let progressRateHeart = document.querySelector(".progressRateHeart");
// MSG < 80%
let heart80 = document.createElement("div");
heart80.classList.add("msgHeart");
heart80.innerText = "Panama a besoin d'attention";
// MSG < 20%
let heart20 = document.createElement("div");
heart20.classList.add("msgHeart");
heart20.innerText = "Panama veux un calin !";
// COLORS
let colorsHeart = ["#d02035", "#d43649", "#d94c5d", "#de6271"];

////// COFFEE PROGRESS BAR VARIABLES //////

let widthCoffee = 100;
let progressCoffee = document.querySelector(".progressCoffee");
let progressRateCoffee = document.querySelector(".progressRateCoffee");
// MSG < 80%
let coffee80 = document.createElement("div");
coffee80.classList.add("msgCoffee");
coffee80.innerText = "Panama aimerait un café";
// MSG < 20%
let coffee20 = document.createElement("div");
coffee20.classList.add("msgCoffee");
coffee20.innerText = "Panama réclame de la caffeine !";
// COLORS
let colorsCoffee = ["#b7763a", "#be834d", "#c59161", "#cc9f75"];

////// CODE PROGRESS BAR VARIABLES //////

let widthCode = 100;
let progressCode = document.querySelector(".progressCode");
let progressRateCode = document.querySelector(".progressRateCode");
// MSG < 80%
let code80 = document.createElement("div");
code80.classList.add("msgCode");
code80.innerText = "Panama souhaite coder";
// MSG < 20%
let code20 = document.createElement("div");
code20.classList.add("msgCode");
code20.innerText = "Panama à besoin de coder du Js !";
// COLORS
let colorsCode = ["#5091ba", "#619cc0", "#72a7c7", "#84b2ce"];

////// OBJECTS //////

let heart = {
    width: widthHeart,
    bar: progressHeart,
    rate: progressRateHeart,
    msg80: heart80,
    msg20: heart20,
    colors: colorsHeart,
    id: "idHeart"
};

let coffee = {
    width: widthCoffee,
    bar: progressCoffee,
    rate: progressRateCoffee,
    msg80: coffee80,
    msg20: coffee20,
    colors: colorsCoffee,
    id: "idCoffee"
};

let code = {
    width: widthCode,
    bar: progressCode,
    rate: progressRateCode,
    msg80: code80,
    msg20: code20,
    colors: colorsCode,
    id: "idCode"
};

////// FUNCTION INTERVAL PROGRESS BAR //////

function startProgressBar() {

        idHeart = setInterval(function() {
            progressBar(heart);
        }, 1000)
        
        idCoffee = setInterval(function() {
            progressBar(coffee);
        }, 900)
        
        idCode = setInterval(function() {
            progressBar(code);
        }, 800)
};

////// FUNCTION UNIVERSAL PROGRESS BAR //////

function progressBar(object) {

    if (object.width > 60) {
        object.width -= 10;
        object.bar.style.backgroundColor = object.colors[0];
    }
    else if (object.width >= 60) {
        object.width -= 5;
        gameContent.appendChild(object.msg80);
    }
    else if (object.width > 15) {
        object.width -= 5;
        object.bar.style.backgroundColor = object.colors[2];
    }
    else if (object.width >= 15) {
        object.width --
        gameContent.appendChild(object.msg20);
    }
    else if (object.width >= 1) {
        object.width --
        object.bar.style.backgroundColor = object.colors[3];
    }
    else if (object.width === 0) {
        clearInterval(object.id);
    }

    object.bar.style.width = object.width + "%";
    object.rate.innerText = object.width + "%";
};

////// VARIABLES ADD FUNCTION //////

let buttonHeart = document.querySelector(".buttonHeart");
let buttonCoffee = document.querySelector(".buttonCoffee");
let buttonCode = document.querySelector(".buttonCode");

buttonHeart.addEventListener("click", function() {
    add(heart)
});
buttonCoffee.addEventListener("click", function() {
    add(coffee)
});
buttonCode.addEventListener("click", function() {
    add(code)
});

////// FUNCTION UNIVERSAL ADD //////

let score = 0; // +1 FOR BUTTON +2 FOR COMMAND

// WHEN CLICK ON ADD BUTTON
function add(object) {
    if (object.width > 0 && object.width < 100) {
        object.width += 10;
        score = score + 1;
    }
    else {
    }
};

////// VARIABLE UNIVERSAL MSG //////

let gameContent = document.querySelector(".gameContent");

////// VARIABLES GUIDE MSG FUNCTION //////

let guideMsg = document.querySelector(".guideMsg");

////// FUNCTION GUIDE MSG //////

// WHEN CLICK ON NAVBAR GUIDE BUTTON
function guideMsgDisplay() {
    guideMsg.style.display = "block";
    gameContent.appendChild(guideMsg);
}

////// VARIABLE COMMAND FUNCTION //////

let inputCommand = document.querySelector(".inputCommand");

// MSG LOVE
let loveCommand = document.createElement("div");
loveCommand.classList.add("commandMsg");
loveCommand.innerText = "Vous donnez de l'amour à Panama";
// MSG COFFEE
let coffeeCommand = document.createElement("div");
coffeeCommand.classList.add("commandMsg");
coffeeCommand.innerText = "Vous apportez du café à Panama";
// MSG CODE
let codeCommand = document.createElement("div");
codeCommand.classList.add("commandMsg");
codeCommand.innerText = "Vous codez du Js pour Panama";
// MSG ERREUR
let erreurCommand = document.querySelector(".erreurMsg");
// MSH HELP
let helpCommand = document.querySelector(".helpMsg");

////// FUNCTION COMMAND //////

inputCommand.addEventListener("keydown", function(playerCommand) {
    if (playerCommand.keyCode === 13) {
        let playerCommand = inputCommand.value.toString();
        if (playerCommand === "/love") {
            add(heart);
            gameContent.appendChild(loveCommand);
            score = score + 2;
        }
        else if (playerCommand === "/coffee") {
            add(coffee);
            gameContent.appendChild(coffeeCommand);
            score = score + 2;
        }
        else if (playerCommand === "/code") {
            add(code);
            gameContent.appendChild(codeCommand);
            score = score + 2;
        }
        else if (playerCommand === "/help") {
            helpCommand.style.display = "block";
            gameContent.appendChild(helpCommand);
            score = score + 2;
        }
        else {
            erreurCommand.style.display = "block";
            gameContent.appendChild(erreurCommand);
            score = score + 2;
        }
    }
});

////// VARIABLE DEAD FUNCTION //////

// FRAME ALIVE
let frame0 = document.getElementById("frame0");
let frame1 = document.getElementById("frame1");
let frame2 = document.getElementById("frame2");
let frame3 = document.getElementById("frame3");
let frame4 = document.getElementById("frame4");
let frame5 = document.getElementById("frame5");

// FRAME DEAD
let dead0 = document.getElementById("dead0");
let dead1 = document.getElementById("dead1");

////// MSG GAME OVER ///////

// GAME OVER MSG
let gameOver = document.querySelector(".gameOver");
// SCORE MSG
let lastScoreMsg = document.querySelector(".lastScore");
let newScoreMsg = document.querySelector(".newScore");
// LAST SCORE MSG
let lastScore = localStorage.getItem("lastScore");
lastScoreMsg.innerHTML = `last game : ${lastScore}`;
// RELOAD BUTTON MSG
let gameOverButton = document.querySelector(".gameOverButton");
// WHEN CLICK ON GAME OVER MSG BUTTON
gameOverButton.addEventListener("click", function() {
    document.location.reload(true);
})

////// FUNCTION DEAD //////

function dead(object1, object2, object3) {
    if (object1.width === 0 && object2.width === 0 && object3.width === 0) {
        clearInterval(idDead);
        // DINOSAUR DISAPPEAR
        frame0.style.display = "none";
        frame1.style.display = "none";
        frame2.style.display = "none";
        frame3.style.display = "none";
        frame4.style.display = "none";
        frame5.style.display = "none";
        // GHOST APPEAR
        dead0.style.display = "block";
        dead1.style.display = "block";
        // GAME OVER MSG
        gameOver.style.display= "block";
        gameContent.appendChild(gameOver);
        // NEW SCORE MSG
        newScoreMsg.innerHTML = `score : ${score}`;
        // PUSH LOCAL STORAGE SCORE
        localStorage.setItem("lastScore", score);
    }
    else {
    }
}