# TP 4 - Tamagotchi

**A rendre pour le 23/07/2020 au soir**

[Lien vers la maquette](https://www.figma.com/file/IPPVQqT9BNLe7yMiMLxVRk/Maquette-Tamagotchi?node-id=0%3A1)

### Consignes :

L’objectif est de réalisé une page Web dynamique contenant un petit compagnon virtuel, à l’instar du fameux Tamagotchi

### Points obligatoires :

* realiser en HTML5, CSS3
* responsivité, respect des règles d'accessibilités et optimisé pour le SEO
* utiliser SASS pour la création de votre CSS
* vous devez utiliser une convention de nommage ( camelCase )

#### V1 :

1. Avoir minimum 3 besoins, ces besoin vont diminuer progressivement dans le temps. les besoins doivent être visible à tout moment par l'utilisateur.
2. Quand les 3 besoins tombent à zéro, votre compagnon s'évanouit et la partie est perdue.
3. Chaque besoin peut être alimenté en cliquant sur des bouutons correspondants. Les besoins ne peuvent pas dépasser un maximum.
4. Un texte d'aide rappelera les différentes règles du jeu.

#### V2 :

1. Les besoins seront remplis par la réponse à une commande écrite, comme dans un terminal de commande.
2. Vous devez mettre en place un champ d'input, pour entrer les commandes, et d'un output pour afficher les résultats et les infos.
3. Il faut une commande pour chaque besoin et une commande d'aide pour donner les commandes disponibles. Une commande invalide doit être indiquer à l'utilisateur.